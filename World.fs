
module rec World

type point =
  { x : int
    y : int
    z : int
  }

type chunking =
  { rough : point  // chunk id
    local : point  // coords inside a chunk
  }

let rough_point size point =
  let mask = size - 1
  { x = point.x &&& ~~~mask
    y = point.y &&& ~~~mask
    z = point.z &&& ~~~mask
  }

let local_point size point =
  let mask = size - 1
  { x = point.x &&& mask
    y = point.y &&& mask
    z = point.z &&& mask
  }

let distance a b =
  let sqr x = x * x
  sqr (a.x - b.x) + sqr (a.y - b.y) + sqr (a.z - b.z)
  |> float
  |> sqrt
  |> int

let filename pt = sprintf "save/%A_%A_%A.chunk" pt.x pt.y pt.z

type 'a chunk =
  | Filled   of 'a              // whole NxNxN box filled with one value
  | Detailed of bool * 'a [,,]  // box of random content, bool is a "dirty" flag

type 'a world =
  { mutable chunks : (point, 'a chunk) Map       // is mutable for delayed loading
    loci           :  point list                 // where character are
    load_chunk     :  point -> 'a chunk          // load/generate chunk
    save_chunk     :  point -> 'a chunk -> unit  // save chunk
    chunk_size     :  int
    view_distance  :  int                        // in whole chunks
    tick           :  int64                      // I dunno, time? Will need.
  }

let is_dirty chunk =
  match chunk with
  | Detailed (true, _) -> true
  | _                  -> false

// Return a list of points that are inside a sphere.
//
let flood_fill_sphere delta radius center =
  let rec loop visited stack =
    match stack with
    | pt :: rest ->
      if Set.contains pt visited || distance center pt > radius * delta
      then loop visited rest
      else
        let next =
          [ { pt with x = pt.x + delta }
            { pt with x = pt.x - delta }
            { pt with y = pt.y + delta }
            { pt with y = pt.y - delta }
            { pt with z = pt.z + delta }
            { pt with z = pt.z - delta }
          ]

        loop (Set.add pt visited) (List.append next rest)
    | [] ->
      visited

  loop Set.empty [center]

// MS, why the fuck is this NOT in the stdlib?
//
let lookup key map =
  if Map.containsKey key map
  then Some map.[key]
  else None

// Read a batch of blocks.
//
let get_batch
  (world : 'a world)
  (batch :  point list)
         : (point, 'a) Map
 =
  let SIZE           = world.chunk_size
  let chunk_of point = rough_point SIZE point
  let by_chunks      = List.groupBy chunk_of batch

  let mutable acc = []

  for (rough, sub_batch) in by_chunks do
    let chunks = world.chunks
    let chunk =
      match lookup rough chunks with
      | None ->
        let new_chunk = world.load_chunk rough
        world.chunks <- Map.add rough new_chunk world.chunks
        new_chunk

      | Some chunk ->
        chunk

    let retrieve pt =
      match chunk with
      | Filled a ->
        (pt, a)

      | Detailed (_, array) ->
        let local = local_point SIZE pt
        (pt, array.[local.x, local.y, local.z])

    acc <- List.append (List.map retrieve sub_batch) acc

  Map.ofList acc

// Write a batch of blocks.
//
let set_batch
  (world : 'a world)
  (batch : (point * 'a) list)
         :  unit
 =
  let SIZE = world.chunk_size
  let chunk_of (point, _) = rough_point SIZE point
  let by_chunks = List.groupBy chunk_of batch

  for (rough, sub_batch) in by_chunks do
    let chunks = world.chunks
    let chunk =
      match lookup rough chunks with
      | None       -> world.load_chunk rough
      | Some chunk -> chunk

    let prepared_chunk =
      match chunk with
      | Filled a ->
        let array = Array3D.create SIZE SIZE SIZE a
        Detailed (true, array)

      | Detailed (_, array) ->
        Detailed (true, array)

    for (point, block) in sub_batch do
      let local = local_point SIZE point

      match prepared_chunk with
      | Detailed (_, array) ->
        array.[local.x, local.y, local.z] <- block

    world.chunks <- Map.add rough prepared_chunk world.chunks

// UUGH!
//
let Map_keys map = Map.toSeq map |> Seq.map fst |> Set.ofSeq

// Load, save and remove from world all the neccessary chunks.
//
let update_chunks
  (world   : 'a world)
           :    unit
 =
  let SIZE              = world.chunk_size
  let rough_centers     = List.map (rough_point SIZE) world.loci
  let spheres           = List.map (flood_fill_sphere SIZE world.view_distance) rough_centers
  let all_nearby        = List.reduce Set.union spheres
  let (there, missing)  = Set.partition (fun pt -> Map.containsKey pt world.chunks) all_nearby
  let keys              = Map_keys world.chunks
  let (fine, excessive) = Set.partition (fun pt -> Set.contains pt there) keys

  let dirty_chunks = query {
    for pt in fine do
      let chunk = world.chunks.[pt]
      where  (is_dirty chunk)
      select (pt, chunk)
  }

  for pt in missing do
    let loaded = world.load_chunk pt
    world.chunks <- Map.add pt loaded world.chunks

  for pt in excessive do
    world.save_chunk pt world.chunks.[pt]

  for (pt, chunk) in dirty_chunks do
    world.save_chunk pt chunk

  world.chunks <- Map.filter (fun pt _ -> not (Set.contains pt excessive)) world.chunks

// Creating formatter for all save/load.
//
let formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter()

// Save chunk.
//
let save pt chunk =
  let fout = System.IO.File.Open (filename(pt), System.IO.FileMode.Create)
  formatter.Serialize(fout, chunk)
  fout.Close()

// Load chunk.
//
let load (or_gen_at : point -> 'a chunk) (pt : point) : 'a chunk =
  try
    let fin = System.IO.File.Open (filename(pt), System.IO.FileMode.Open, System.IO.FileAccess.Read)

    // v-- cast from "obj", may fail
    downcast (formatter.Deserialize fin)
  with e ->
    or_gen_at pt

// Create empty test world.
//
let void_world air =
  { chunks = Map.empty
    loci   = [{x = 0; y = 0; z = 0}]

    load_chunk = load <| fun pt -> Filled air
    save_chunk = save

    view_distance = 5
    chunk_size    = 8
    tick          = 0L
  }

let w = void_world 0

printf "u"
update_chunks w
printfn "pdate"

set_batch w [{x = -40; y = -8; z = 1}, 6]

printfn "get_batch: %A" <| get_batch w [{x = -40; y = -8; z = 1}]

printf "u"
update_chunks w
printfn "pdate"

printf "u"
update_chunks { w with loci = [{x = 8; y = 8; z = 8}]}
printfn "pdate"

printfn "World: %A" <| w